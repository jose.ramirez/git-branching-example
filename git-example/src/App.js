import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

//Components
import Card from './components/Card/Card';
import Footer from './components/Footer/Footer';


function App() {
  return (
    <div className="App">
      <div className="container">
        <nav className="navbar navbar-expand-sm bg-light">
          <a className="navbar-brand" href="/">
            <img src={logo} width="30" height="30" alt="Joche Ramirez"></img>
          </a>
          <a href="/" className="navbar-brand">Home</a>
          <div className="navbar navbar-expand-sm bg-light">
            <ul className="navbar-nav">
              <li className="nav-item">
                <a href="/" className="nav-link">Todos</a>
              </li>
              <li className="nav-item">
                <a href="/" className="nav-link">Create</a>
              </li>
            </ul>
          </div>
        </nav>
        <br />
      </div>
      <Card title="First" description="This is the very first card of the app"/>
      <Card title="Second" description="This is the second card of the app"/>
      <Card title="Third" description="This is the third card of the app"/>
      <Footer />
    </div>
  );
}

export default App;
