import React, { Component } from 'react';
import './Card.css';

export default class Card extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div id="card">
                <h4>{this.props.title}</h4>
                <p>{this.props.description}</p>
                <input type="button" value="Like"/>
                <br/>
            </div>
        )
    }
}